<?php
declare(strict_types=1);

/** @var $app Slim\App */

$app->group('/api', function () use ($app) {

    $app->group('/auth', function () use ($app) {
        $app->post('', 'ApiController:auth');
        $app->delete('', 'ApiController:deauth');

        $app->post('/renew', 'ApiController:renew');
    });

    $app->group('', function () use ($app) {
        $app->post('/password', 'ApiAccountController:updatePassword');
        $app->post('/diploma', 'ApiDiplomaController:sendDiploma');
	$app->post('/settings', 'ApiSettingsController:updateSettings');

        $app->group('/stats', function () use ($app) {
            $app->get('/signupsByMonth', 'ApiStatsController:signupsByMonth');
            $app->get('/diplomasByMonth', 'ApiStatsController:diplomasByMonth');
        });

        $app->group('/{type}', function () use ($app) {
            $app->get('', 'ApiCrudController:list');
            $app->post('', 'ApiCrudController:create');
            $app->get('/{id:\d+}', 'ApiCrudController:get');
            $app->put('/{id:\d+}', 'ApiCrudController:update');
            $app->delete('/{id:\d+}', 'ApiCrudController:delete');
        });
    })->add($app->getContainer()->get('AuthorizationMiddleware'));
});
