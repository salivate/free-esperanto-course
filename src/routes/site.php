<?php
declare(strict_types=1);

/** @var $app Slim\App */

$app->get('/admin[/{page:\w+}]', 'PageController:getAdmin');

$app->post('/signup', 'PageController:signup');

$app->get('/robots.txt', 'PageController:getRobots');
$app->get('/{path:.*}', 'PageController:get');
