<?php
declare(strict_types=1);

use Slim\App;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use Dflydev\FigCookies\Cookie;
use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;
use Firebase\JWT\JWT;
use setasign\Fpdi\Fpdi;
use FEC\Middleware\AuthorizationMiddleware;
use FEC\Models\Accounts;
use FEC\Models\Authentication;
use FEC\Models\Pages;
use FEC\Models\Settings;
use FEC\Models\Stats;
use FEC\Controllers\AdminController;
use FEC\Controllers\ApiController;
use FEC\Controllers\PageController;
use FEC\Controllers\Api\CrudController as ApiCrudController;
use FEC\Controllers\Api\AccountController as ApiAccountController;
use FEC\Controllers\Api\DiplomaController as ApiDiplomaController;
use FEC\Controllers\Api\StatsController as ApiStatsController;
use FEC\Controllers\Api\SettingsController as ApiSettingsController;

// All object instantiation and initialization should happen in this file.

define('FPDF_FONTPATH', './diploma');

$c = new Container(['settings' => [
    'displayErrorDetails' => getenv('DEBUG')
]]);

$c['App'] = function (Container $c): App {
    $app = new App($c);
    $app->view['site'] = $c;

    require_once 'routes/api.php';
    require_once 'routes/site.php';

    return $app;
};

$c['db'] = function (Container $c): PDO {
    $pdo = new PDO(
        $c->environment['DB_DSN'],
        $c->environment['DB_USERNAME'],
        $c->environment['DB_PASSWORD']
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($pdo->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') {
        $pdo->query('SET SESSION sql_mode = PIPES_AS_CONCAT');
    }

    return $pdo;
};

$c['Diploma'] = function (Container $c): Fpdi {
    $pdf = new Fpdi('l', 'mm', 'letter');

    $pdf->addFont('Century751BT-BoldB');
    $pdf->setFont('Century751BT-BoldB');

    $pdf->setSourceFile('./diploma/diploma.pdf');

    $pdf->addPage();
    $template = $pdf->importPage(1);
    $pdf->useTemplate($template);

    return $pdf;
};

$c['JWTEncoder'] = function (Container $c) {
    return function (array $token) use ($c): string {
        return JWT::encode($token, $c->environment['JWT_SECRET']);
    };
};

$c['JWTDecoder'] = function (Container $c) {
    return function (string $token) use ($c): array {
        return (array)JWT::decode($token, $c->environment['JWT_SECRET'], ['HS256']);
    };
};

$c['Mailer'] = function (Container $c): Swift_Mailer {
    $transport = new Swift_SmtpTransport(
        $c->environment['SMTP_HOST'],
        $c->environment['SMTP_PORT']
    );
    return new Swift_Mailer($transport);
};

$c['Message'] = $c->factory(function (Container $c): Swift_Message {
    $msg = new Swift_Message;
    $msg->setSender(
        $c->environment['EMAIL_SYSTEM_ADDRESS'],
        $c->environment['EMAIL_SYSTEM_NAME']
    );
    return $msg;
});

$c['MessageAttachment'] = $c->factory(function (Container $c): Swift_Attachment {
    return new Swift_Attachment;
});

$c['RequestCookie'] = function (Container $c) {
    return function (Request $req, string $name) use ($c): Cookie {
        return FigRequestCookies::get($req, $name);
    };
};

$c['SetResponseCookie'] = function (Container $c) {
    return function (
        Response $resp,
        string $name,
        string $value,
        int $expires = 0,
        string $path = "",
        string $domain = "",
        bool $secure = false,
        bool $httpOnly = false
    ) use ($c): Response {
        return FigResponseCookies::set(
            $resp,
            SetCookie::create($name)
                ->withValue($value)
                ->withExpires($expires)
                ->withPath($path)
                ->withDomain($domain)
                ->withSecure($secure)
                ->withHttpOnly($httpOnly)
        );
    };
};

$c['Twig'] = function (Container $c): Twig {
    $twig = new Twig('./templates', ['debug' => $c->environment['DEBUG']]);
    return $twig;
};

// middleware
$c['AuthorizationMiddleware'] = function (Container $c): AuthorizationMiddleware {
    return new AuthorizationMiddleware($c);
};

// models
$c['Accounts'] = function (Container $c): Accounts {
    return new Accounts($c);
};
$c['Authentication'] = function (Container $c): Authentication {
    return new Authentication($c);
};
$c['Pages'] = function (Container $c): Pages {
    return new Pages($c);
};
$c['Settings'] = function (Container $c): Settings {
    return new Settings($c);
};
$c['Stats'] = function (Container $c): Stats {
    return new Stats($c);
};

// controllers
$c['AdminController'] = function (Container $c): AdminController {
    return new AdminController($c);
};
$c['ApiController'] = function (Container $c): ApiController {
    return new ApiController($c);
};
$c['ApiCrudController'] = function (Container $c): ApiCrudController {
    return new ApiCrudController($c);
};
$c['ApiAccountController'] = function (Container $c): ApiAccountController {
    return new ApiAccountController($c);
};
$c['ApiDiplomaController'] = function (Container $c): ApiDiplomaController {
    return new ApiDiplomaController($c);
};
$c['ApiSettingsController'] = function (Container $c): ApiSettingsController {
    return new ApiSettingsController($c);
};
$c['ApiStatsController'] = function (Container $c): ApiStatsController {
    return new ApiStatsController($c);
};
$c['PageController'] = function (Container $c): PageController {
    return new PageController($c);
};

foreach ($c->get('Settings')->get() as $row) {
    $c->environment[$row['param']] = $row['value'];
}

return $c;
