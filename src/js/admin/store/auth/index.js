import jwt_decode from "jwt-decode";

export default {
    namespaced: true,

    state: {
        authToken: "",
        decodedAuthToken: {}
    },

    getters: {
        authToken: (state) => {
            return state.authToken;
        },

        isAuthTokenExpired: (state) => {
            return !(state.decodedAuthToken &&
                state.decodedAuthToken.exp &&
                state.decodedAuthToken.exp > (Date.now() / 1000));
        },

        isUserAdmin: (state) => {
            return (state.decodedAuthToken &&
                state.decodedAuthToken.account &&
                state.decodedAuthToken.account.role &&
                state.decodedAuthToken.account.role == "admin");
        }

    },

    actions: {
        setAuthToken: ({commit}, authToken) => {
            let decodedAuthToken;
            return new Promise((resolve, reject) => {
                try {
                    decodedAuthToken = jwt_decode(authToken);
                } catch (error) {
                    reject(error);
                }

                commit("setAuthToken", authToken);
                commit("setDecodedAuthToken", decodedAuthToken);
                resolve();
            });
        },
    
        clearAuthToken: ({commit}) => {
            return new Promise((resolve) => {
                commit("setAuthToken", "");
                commit("setDecodedAuthToken", {});
                resolve();
            });
        }
    },
    
    mutations: {
        setAuthToken: (state, authToken) => {
            state.authToken = authToken;
        },

        setDecodedAuthToken: (state, decodedAuthToken) => {
            state.decodedAuthToken = decodedAuthToken;
        }
    }
};
