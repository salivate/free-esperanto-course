export default {
    namespaced: true,

    state: {
        isLoading: false
    },

    getters: {
        isLoading: (state) => {
            return state.isLoading;
        }
    },

    actions: {
        setIsLoading: ({commit}, isLoading) => {
            return new Promise((resolve) => {
                commit("setIsLoading", isLoading);
                resolve();
            });
        }
    },
    
    mutations: {
        setIsLoading: (state, isLoading) => {
            state.isLoading = isLoading;
        }
    }
};
