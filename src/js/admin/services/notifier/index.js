const duration = 500;

export default {
    send(message) {
        localStorage.setItem(message, true);
        setInterval(() => {
             localStorage.removeItem(message);
        }, duration);
    }
};
