import axios from "axios";
import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
import LoginView from "../views/Login";
import IndexView from "../views/Index";
import PagesView from "../views/Pages";
import AccountsView from "../views/Accounts";
import SettingsView from "../views/Settings";
import DiplomasView from "../views/Diplomas";
import StatsView from "../views/Stats";
import PasswordView from "../views/Password";
import NotFoundView from "../views/NotFound";

Vue.use(VueRouter);

const isAuthenticated = () => {
    if (!store.getters["auth/isAuthTokenExpired"]) {
        return Promise.resolve(true);
    }

    // attempt to renew the authToken 
    return axios.post("/api/auth/renew")
        .then((response) => {
            return store.dispatch("auth/setAuthToken", response.data.authToken);
        })
        .then(() => {
            return Promise.resolve(true);
        })
        .catch((error) => {
            return Promise.resolve(false);
        });
};

const requireAuth = async (to, from, next) => {
    if (await isAuthenticated()) {
        next();
        return;
    }
    next({name: "login", params: {internal: true}});
};

const prohibitAuth = async (to, from, next) => {
    // never restrict if "internal"
    if ((to.params && to.params.internal) || !(await isAuthenticated())) {
        next();
        return;
    }
    next({name: "index"});
}

export default new VueRouter({
    mode: "history",
    routes: [
        {
            name: "login",
            path: "/admin/login",
            component: LoginView,
            beforeEnter: prohibitAuth
        },
        {
            name: "index",
            path: "/admin",
            component: IndexView,
            beforeEnter: requireAuth
        },
        {
            name: "pages",
            path: "/admin/pages",
            component: PagesView,
            beforeEnter: requireAuth
        },
        {
            name: "accounts",
            path: "/admin/accounts",
            component: AccountsView,
            beforeEnter: requireAuth
        },
        {
            name: "settings",
            path: "/admin/settings",
            component: SettingsView,
            beforeEnter: requireAuth
        },
        {
            name: "diplomas",
            path: "/admin/diplomas",
            component: DiplomasView,
            beforeEnter: requireAuth
        },
        {
            name: "stats",
            path: "/admin/stats",
            component: StatsView,
            beforeEnter: requireAuth
        },
        {
            name: "password",
            path: "/admin/password",
            component: PasswordView,
            beforeEnter: requireAuth
        },
        {
            name: "not-found",
            path: "*",
            component: NotFoundView
        }
    ]
});
