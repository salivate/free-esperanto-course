<?php
declare(strict_types=1);

if (PHP_SAPI == 'cli-server') {
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $path = realpath(__DIR__ . $path);
    if ($path !== false && is_file($path) && strpos($path, __DIR__) === 0) {
        return false;
    }
}

chdir(__DIR__ . '/..');
require_once 'vendor/autoload.php';

(new Dotenv\Dotenv('.'))->load();
(require_once 'dependencies.php')->App->run();
