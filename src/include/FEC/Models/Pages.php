<?php
declare(strict_types=1);

namespace FEC\Models;

use Slim\Container;
use Boronczyk\Alistair\CrudModel;

/**
 * Class Pages
 * @package FEC\Models
 */
class Pages extends CrudModel
{
    protected $twig;

    public function __construct(Container $c)
    {
        parent::__construct($c->db);
        $this->twig = $c->get('Twig');
    }

    public function columns(): array
    {
        return ['path', 'title', 'description', 'content', 'template'];
    }

    /**
     * Return a list of pages for generating sitemap.xml
     *
     * @return array
     */
    public function getForSitemap(): array
    {
        $rows = $this->queryRows(
            'SELECT id, path, title, updated FROM pages ORDER BY path ASC'
        );
        return $rows;
    }

    /**
     * Return a page by URL path.
     *
     * This is used for page rendering.
     *
     * @param string $path
     * @return array|null
     */
    public function getByPath(string $path): ?array
    {
        return $this->queryRow(
            'SELECT id, path, title, description, content, template, updated
            FROM pages WHERE path = ?',
            [$path]
        );
    }

    /**
     * Replaces {{placeholders}} found in the given content.
     *
     * Placeholders are base names of files without extension found in the
     * templates directory. Those that do not resolve to a file (the file
     * doesn't exist) are left un-replaced.
     *
     * @param string $content
     * @param array $data
     * @return string
     */
    public function replaceTags(string $content, array $data)
    {
        $twig = $this->twig;

        // Placeholders are base names of files without extension found in the
        // templates directory. Those that do not resolve to a file (the file
        // doesn't exist) are left un-replaced.
        return preg_replace_callback(
            '/\{\{([^}]+)\}\}/',
            function (array $arg) use ($twig, $data): string {
                $file = 'site/' . trim($arg[1]) . '.html';
                return (file_exists("templates/$file")) ?
                    $twig->fetch($file, $data) : $arg[0];
            },
            $content
        );
    }
}
