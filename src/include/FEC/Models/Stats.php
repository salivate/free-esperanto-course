<?php
declare(strict_types=1);

namespace FEC\Models;

use Slim\Container;
use Boronczyk\Alistair\DbAccess;

/**
 * Class Stats
 * @package FEC\Models
 */
class Stats extends DbAccess
{

    protected $year;
    protected $month;

    public function __construct(Container $c)
    {
        parent::__construct($c->db);

    $this->year = date('Y');
    $this->month = date('m');
    }

    /**
     * Record student signups.
     *
     * @param array
     */
    public function recordSignup(array $data)
    {
        $this->query(
            'INSERT INTO stats_signup (`year`, `month`, `count`) VALUES (?, ?, 1)
             ON DUPLICATE KEY UPDATE `count` = `count` + 1',
            [$this->year, $this->month]
        );
    }

    /**
     * Report the number of signups by month for the past year.
     *
     * @return array
     */
    public function signupsByMonth(): array
    {
        $data = [];
        for ($month = $this->month; $month > 0; $month--) {
            $data[sprintf('%d%02d', $this->year, $month)] = [
                'year' => $this->year,
                'month' => $month,
                'count' => 0
            ];
        }
    
        $year = $this->year - 1;
        for ($month = 12; $month >= $this->month; $month--) {
            $data[sprintf('%d%02d', $year, $month)] = [
                'year' => $year,
                'month' => $month,
                'count' => 0
            ];
        }
    
        $rows = $this->queryRows(
            'SELECT `year`, `month`, `count` FROM stats_signup
             WHERE (`year` = ? AND `month` <= ?) 
             OR (`year` = ? AND `month` >= ?)',
            [
                 $this->year, 
                 $this->month,
                 $year,
                 $this->month
            ]
        );

        foreach ($rows as $row) {
            $data[sprintf('%d%02d', $row['year'], $row['month'])] = $row;
        }

        array_walk($data, function (&$row) {
            settype($row['year'], 'int');
            settype($row['month'], 'int');
            settype($row['count'], 'int');
        });

        return array_values($data);
    }

    /**
     * Record issued diplomas.
     *
     * @param array
     */
    public function recordDiploma(array $data)
    {
        $this->query(
            'INSERT INTO stats_diploma (`year`, `month`, `count`) VALUES (?, ?, 1)
             ON DUPLICATE KEY UPDATE `count` = `count` + 1',
            [$this->year, $this->month]
        );

        $this->query(
            'INSERT INTO diplomas (id, name, email, tutor) VALUES (NULL, ?, ?, ?)',
            [$data['name'], $data['email'], $data['tutor']]
        );
    }

    /**
     * Report the number of diplomas issued by month for the past year.
     *
     * @param array
     */
    public function diplomasByMonth(): array
    {
        $data = [];
        for ($month = $this->month; $month > 0; $month--) {
            $data[sprintf('%d%02d', $this->year, $month)] = [
                'year' => $this->year,
                'month' => $month,
                'count' => 0
            ];
        }
    
        $year = $this->year - 1;
        for ($month = 12; $month >= $this->month; $month--) {
            $data[sprintf('%d%02d', $year, $month)] = [
                'year' => $year,
                'month' => $month,
                'count' => 0
            ];
        }
    
        $rows = $this->queryRows(
            'SELECT `year`, `month`, `count` FROM stats_diploma
             WHERE (`year` = ? AND `month` <= ?) 
             OR (`year` = ? AND `month` >= ?)',
            [
                 $this->year, 
                 $this->month,
                 $year,
                 $this->month
            ]
        );

        foreach ($rows as $row) {
            $data[sprintf('%d%02d', $row['year'], $row['month'])] = $row;
        }

        array_walk($data, function (&$row) {
            settype($row['year'], 'int');
            settype($row['month'], 'int');
            settype($row['count'], 'int');
        });

        return array_values($data);
    }
}
