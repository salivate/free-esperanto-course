<?php
declare(strict_types=1);

namespace FEC\Models;

use Slim\Container;
use Boronczyk\Alistair\DbAccess;

/**
 * Class Authentication
 * @package FEC\Models
 */
class Authentication extends DbAccess
{

    protected $refreshExpire = 60 * 15; // 15 minutes

    public function __construct(Container $c)
    {
        parent::__construct($c->db);
    }

    /**
     * Authenticate an account.
     *
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function authenticate(string $email, string $password): bool
    {
        $pswd = $this->queryValue(
            'SELECT password FROM accounts WHERE email = ?',
            [$email]
        );
        return ($pswd && password_verify($password, $pswd));
    }

    /**
     * Create and save a refresh token.
     *
     * @param array $account
     * @param int $expire
     * @return string
     */
    public function makeRefreshToken(array $account, int $expire)
    {
        $token = bin2hex(random_bytes(8));

        $this->query(
            'INSERT INTO refresh_tokens (account_id, token, expires)
             VALUES (?, ?, ?)',
            [$account['id'], $token, $expire]
        );

        return $token;
    }

    /**
     * Retrieve details for a refresh token.
     *
     * @param string $token
     * @return rray
     */
    public function fetchRefreshInfo(string $token): array
    {
        return $this->queryRow(
            'SELECT account_id, token, expires FROM refresh_tokens WHERE token = ?
             ORDER BY expires DESC LIMIT 1',
            [$token]
        );
    }

    /**
     * Remove details for a refresh token.
     *
     * @param string $token
     */
    public function deleteRefreshInfo(string $token)
    {
        return $this->query(
            'DELETE FROM refresh_tokens WHERE token = ?',
            [$token]
        );
    }
}
