<?php
declare(strict_types=1);

namespace FEC\Models;

use Slim\Container;
use Boronczyk\Alistair\CrudModel;

/**
 * Class Settings
 * @package FEC\Models
 */
class Settings extends CrudModel
{
    public function __construct(Container $c)
    {
        parent::__construct($c->db);
    }

    public function columns(): array
    {
        return ['param', 'description', 'type', 'value'];
    }

    /**
     * Return a setting by param.
     *
     * @param string $param
     * @return array|null
     */
    public function getByParam(string $param): ?array
    {
        return $this->queryRow(
            "SELECT id, {$this->columnsAsList()} FROM settings
             WHERE `param` = ?",
            [$param]
        );
    }

    /**
     * Update multiple settings.
     *
     * @param array $settings
     */
    public function updateBulk(array $settings)
    {
        foreach ($settings as $setting) {
            $this->query(
                "UPDATE settings SET `value` = ? WHERE `param` = ?",
                [$setting['value'], $setting['param']]
            );
        }
    }
}
