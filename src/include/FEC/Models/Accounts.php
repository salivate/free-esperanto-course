<?php
declare(strict_types=1);

namespace FEC\Models;

use Slim\Container;
use Boronczyk\Alistair\CrudModel;

/**
 * Class Accounts
 * @package FEC\Models
 */
class Accounts extends CrudModel
{
    public function __construct(Container $c)
    {
        parent::__construct($c->db);
    }

    public function columns(): array
    {
        return ['first_name', 'last_name', 'email', 'role', 'available'];
    }

    /**
     * Return an account by email address.
     *
     * @param string $email
     * @return array|null
     */
    public function getByEmail(string $email): ?array
    {
        return $this->queryRow(
            "SELECT id, {$this->columnsAsList()} FROM accounts
             WHERE email = ?",
            [$email]
        );
    }

    /**
     * Create a new account record and return the record's ID.
     *
     * @param array $data
     * @return int
     * @throws \PDOException
     */
    public function create(array $data): int
    {
        $id = parent::create($data);

        if (!empty($data['password'])) {
            $this->updatePassword($id, $data['password']);
        }

        return $id;
    }

    /**
     * Update an account's password.
     *
     * @param int $id
     * @param string $password
     */
    public function updatePassword(int $id, string $password)
    {
        $password = password_hash($password, PASSWORD_BCRYPT);

        $this->query(
            'UPDATE accounts SET password = ? WHERE id = ?',
            [$password, $id]
        );
    }

    /**
     * Get next available tutor.
     *
     * @return array
     */
    public function getNextTutor(): array
    {
        $this->db->beginTransaction();

        $lastId = $this->queryValue('SELECT id FROM last_assigned_tutor');
    
        $nextId = $this->queryValue(
            'SELECT MIN(id) FROM accounts WHERE available = 1 AND id > ?',
            [$lastId]
        );
        if (!$nextId) {
            $nextId = $this->queryValue(
                'SELECT MIN(id) FROM accounts WHERE available = 1'
            );
        }
        if (!$nextId) {
            $nextId = $lastId;
        }

        $this->query('UPDATE last_assigned_tutor SET id = ?', [$nextId]);
        $this->db->commit();
    
        return $this->getById((int)$nextId);
    }
}
