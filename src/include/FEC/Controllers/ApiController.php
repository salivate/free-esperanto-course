<?php
declare(strict_types=1);

namespace FEC\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ApiController
 * @package FEC\Controllers
 */
class ApiController extends Controller
{
    /**
     * Authenticate.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function auth(Request $req, Response $resp, array $args): Response
    {
        $auth = $this->container->get('Authentication');

        $data = $req->getParsedBody();
        $email = $data['email'] ?? '';
        $password = $data['password'] ?? '';

        if (!$auth->authenticate($email, $password)) {
            return $this->unauthorizedResponse($resp);
        }

        $accounts = $this->container->get('Accounts');
        $account = $accounts->getByEmail($email);

        $resp = $this->setRefreshTokenCookie($resp, $account);
        return $this->authTokenResponse($resp, $account);
    }

    /**
     * Deauthentication.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function deauth(Request $req, Response $resp, array $args): Response
    {
        $auth = $this->container->get('Authentication');
        $cookies = $this->container->get('RequestCookie');

        $token = (string)$cookies($req, 'refreshToken')->getValue();
        $auth->deleteRefreshInfo($token);

        $resp = $this->expireRefreshTokenCookie($resp);

        return $resp;
    }


    /**
     * Renew authentication.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function renew(Request $req, Response $resp, array $args): Response
    {
        $now = time();

        $auth = $this->container->get('Authentication');
        $cookies = $this->container->get('RequestCookie');

        $token = (string)$cookies($req, 'refreshToken')->getValue();

        $refreshInfo = $auth->fetchRefreshInfo($token);
        if (empty($refreshInfo) || $refreshInfo['expires'] < $now) {
            return $this->unauthorizedResponse($resp);
        }

        $accounts = $this->container->get('Accounts');
        $account = $accounts->getById((int)$refreshInfo['account_id']);

        $minTTL = $now + (2 * 60 * 60); // 2 hours
        if ($refreshInfo['expires'] < $minTTL) {
            $resp = $this->setRefreshTokenCookie($resp, $account);
        }

        return $this->authTokenResponse($resp, $account);
    }

    /**
     * Construct and return an authorization token.
     *
     * @param Response $resp
     * @param array $account
     * @return Response
     */
    protected function authTokenResponse(Response $resp, array $account): Response
    {
        $auth = $this->container->get('Authentication');
        $encoder = $this->container->get('JWTEncoder');

        $now = time();
        $authExpire = $now + (10 * 60); // 10 minutes

        $authToken = $encoder([
            'exp' => $authExpire,
            'account' => $account
        ]);

        return $resp->withJson(['authToken' => $authToken]);
    }

    /**
     * Attach a refresh token cookie to the response.
     *
     * @param Response $resp
     * @param array $account
     * @return Response
     */
    protected function setRefreshTokenCookie(Response $resp, array $account): Response
    {
        $auth = $this->container->get('Authentication');
        $setcookie = $this->container->get('SetResponseCookie');

        $now = time();
        $refreshExpire = $now + (24 * 60 * 60); // 24 hours

        return $setcookie(
            $resp, 
            'refreshToken',
            $auth->makeRefreshToken($account, $refreshExpire),
            $refreshExpire,
            '/api/auth',  // path
            '',           // domain
            false,        // secure
            true          // httpOnly
        );
    }

    /**
     * Expire the refresh token cookie.
     *
     * @param Response $resp
     * @return Response
     */
    protected function expireRefreshTokenCookie(Response $resp): Response
    {
        $auth = $this->container->get('Authentication');
        $setcookie = $this->container->get('SetResponseCookie');

        return $setcookie(
            $resp, 
            'refreshToken',
            '',
            946684800,    // expire time in past
            '/api/auth',  // path
            '',           // domain
            false,        // secure
            true          // httpOnly
        );
    }
}
