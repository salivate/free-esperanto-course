<?php
declare(strict_types=1);

namespace FEC\Controllers;

use Slim\Container;
use Slim\Http\Response;

/**
 * Class Controller
 * @package FEC\Controllers
 */
class Controller
{
    protected $container;
    protected $twig;

    /**
     * Constructor
     *
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        $this->container = $c;
        $this->twig = $c->get('Twig');
    }

    /**
     * Return a bad request response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function badRequestResponse(Response $resp, ?string $msg = null)
    {
        return $resp->withJson(['error' => $msg ?? 'bad request'], 400);
    }

    /**
     * Return a failed captcha response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function captchaFailedResponse(Response $resp, ?string $msg = null)
    {
        return $resp->withJson(['error' => $msg ?? 'captcha failed'], 400);
    }

    /**
     * Return an unauthorized response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function unauthorizedResponse(Response $resp, ?string $msg = null)
    {
        return $resp->withJson(['error' => $msg ?? 'Unauthorized'], 401);
    }

    /**
     * Return a not found response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function notFoundResponse(Response $resp, ?string $msg = null)
    {
        return $resp->withJson(['error' => $msg ?? 'Not found'], 404);
    }

    /**
     * Return a rate limit exceeded response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function rateLimitExceededResponse(Response $resp, ?string $msg = null)
    {
        return $resp->withJson(['error' => $msg ?? 'Rate limit exceeded'], 429);
    }

    /**
     * Return a server error response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function serverErrorResponse(Response $resp, ?string $msg = null)
    {
        return $resp->withJson(['error' => $msg ?? 'Server error occurred'], 500);
    }
}
