<?php
declare(strict_types=1);

namespace FEC\Controllers;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class PageController
 * @package FEC\Controllers
 */
class PageController extends Controller
{
    protected $pages;

    /**
     * Constructor
     *
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        parent::__construct($c);
        $this->pages = $c->get('Pages');
    }

    /**
     * Serve the site admin page
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getAdmin(Request $req, Response $resp, array $args): Response
    {
        return $this->twig->render($resp, 'admin/app.html');
    }

    /**
     * Serve robots.txt
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getRobots(Request $req, Response $resp, array $args): Response
    {
        return $this->twig->render(
            $resp->withHeader('Content-Type', 'text/plain'),
            'site/robots.txt'
        );
    }

    /**
     * Serve sitemap.xml
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getSitemap(Request $req, Response $resp, array $args): Response
    {
        return $this->twig->render(
            $resp->withHeader('Content-Type', 'application/xml'),
            'site/sitemap.txt',
            ['pages' => $this->pages->getForSitemap()]
        );
    }

    /**
     *
     */
    public function signup(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();

        $accounts = $this->container->get('Accounts');
        $tutor = $accounts->getNextTutor();

        $vars = [
            'tutor' => $tutor,
        'lessonsUrl' => sprintf('%s://%s%s',
                $req->getUri()->getScheme(),
                $req->getUri()->getHost(),
                '/lessons'
            )
        ];

        try {
            $message = $this->container->get('Message')
                ->setTo([$data['email'] => $data['name']])
                ->setFrom([$this->container->environment['EMAIL_SYSTEM_ADDRESS'] => $this->container->environment['EMAIL_SYSTEM_ADDRESS']])
                ->setReplyTo([$tutor['email'] => $tutor['first_name'] . ' ' . $tutor['last_name']])
                ->setSubject('[FEC] Welcome to the Free Esperanto Course!')
                ->setBody($this->twig->fetch('mail/welcome.html', $vars), 'text/html')
                ->addPart($this->twig->fetch('mail/welcome.txt', $vars), 'text/plain');

            $this->container->get('Mailer')->send($message);
        } catch (\Exception $e) {
            return $resp->withStatus(400)->write(
                '<p>Unable to send email. Please go back and make sure all form fields are ' .
                'filled properly.</p>'
            );
        }

        $this->container->get('Stats')->recordSignup([
            'name' => $data['name'],
            'email' => $data['email'],
            'tutor' => $tutor['id']
    ]);

        return $resp->withStatus(303)->withHeader('Location', '/welcome');
    }

    /**
     * Serve the requested page.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function get(Request $req, Response $resp, array $args): Response
    {
        $page = $this->pages->getByPath($args['path']);
        if (!$page) {
            // requested page does not exist
            $page = $this->pages->getByPath('404');
            if (!$page) {
                // 404 page doesn't exist!
                return $this->serverErrorResponse($resp);
            }
            $resp = $resp->withStatus(404);
        }

        $page['content'] = $this->pages->replaceTags($page['content'], $page);

        return $this->twig->render($resp, "site/$page[template].html", $page);
    }
}
