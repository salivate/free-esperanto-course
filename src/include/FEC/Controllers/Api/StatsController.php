<?php
declare(strict_types=1);

namespace FEC\Controllers\Api;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use FEC\Controllers\Controller;

/**
 * Class StatsController
 * @package FEC\Controllers\Api
 */
class StatsController extends Controller
{
    protected $stats;

    /**
     * Constructor
     *
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        parent::__construct($c);
        $this->stats = $c->get('Stats');
    }

    /**
     * Report the number of signups by month.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function signupsByMonth(Request $req, Response $resp, array $args): Response
    {
        $signups = $this->stats->signupsByMonth();
        return $resp->withJson($signups);
    }

    /**
     * Report the number of diplomas issued by month.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function diplomasByMonth(Request $req, Response $resp, array $args): Response
    {
        $diplomas = $this->stats->diplomasByMonth();
        return $resp->withJson($diplomas);
    }
}
