<?php
declare(strict_types=1);

namespace FEC\Controllers\Api;

use Slim\Http\Request;
use Slim\Http\Response;
use FEC\Controllers\Controller;

/**
 * Class AccountController
 * @package FEC\Controllers\Api
 */
class AccountController extends CrudController
{
    /**
     * Update the user's password.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function updatePassword(Request $req, Response $resp, array $args): Response
    {
        $account = $req->getAttribute('account');
        $data = $req->getParsedBody();

        $auth = $this->container->get('Authentication');
        if (!$auth->authenticate($account['email'], $data['currentPassword'])) {
            return $this->badRequestResponse($resp);
        }

        $accounts = $this->container->get('Accounts');
        $accounts->updatePassword((int)$account['id'], $data['newPassword']);

        return $resp;
   }

    /**
     * Create a new account in the database.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function create(Request $req, Response $resp, array $args): Response
    {
        $accounts = $this->container->get('Accounts');

        $data = $req->getParsedBody();
        if (!$accounts->hasRequiredFields($data) || empty($data['password'])) {
            return $this->badRequestResponse($resp);
        }

        $id = $accounts->create($data);
        $accounts->updatePassword($id, $data['password']);

        return $resp->withJson(['id' => $id], 201);
    }
}
