<?php
declare(strict_types=1);

namespace FEC\Controllers\Api;

use Slim\Http\Request;
use Slim\Http\Response;
use FEC\Controllers\Controller;

/**
 * Class DiplomaController
 * @package FEC\Controllers\Api
 */
class DiplomaController extends Controller
{
    /**
     * Generate and send a diploma.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function sendDiploma(Request $req, Response $resp, array $args): Response
    {
        $account = $req->getAttribute('account');
        $data = $req->getParsedBody();

        $diploma = $this->generateDiploma($data['name']);

        $attachment = $this->container->get('MessageAttachment')
            ->setFilename('diploma.pdf')
            ->setContentType('application/pdf')
            ->setBody($diploma);

        $message = $this->container->get('Message')
            ->setTo([$data['email'] => $data['name']])
            ->setFrom([$this->container->environment['EMAIL_SYSTEM_ADDRESS'] => $this->container->environment['EMAIL_SYSTEM_NAME']])
            ->setReplyTo([$account['email'] => $account['first_name'] . ' ' . $account['last_name']])
            ->setSubject('[FEC] Gratulojn! Jen via diplomo')
            ->setBody($this->twig->fetch('mail/diploma.html', $data), 'text/html')
            ->addPart($this->twig->fetch('mail/diploma.txt', $data), 'text/plain')
            ->attach($attachment);

        $this->container->get('Mailer')->send($message);

	$this->container->get('Stats')->recordDiploma([
            'name' => $data['name'],
            'email' => $data['email'],
            'tutor' => $account['id']
         ]);

        return $resp;
    }

    protected function generateDiploma(string $name): string
    {
        $months = [
            null, 'januaro', 'februaro', 'marto', 'aprilo', 'majo', 'junio',
            'julio', "a\xFBgusto", 'septembro', 'oktobro', 'novembro', 'decembro'
        ];
	$date = str_replace('x', $months[date('n')], date('j \x Y'));

        $diploma = $this->container->get('Diploma');

        $diploma->SetFontSize('40');
        $diploma->SetXY(10, 115);
        $diploma->Cell(0, 10, $name, 0, 0, 'C');

        $diploma->SetFontSize('14');
        $diploma->SetXY(43, 171);
        $diploma->Cell(75, 10, $date, 0, 0, 'C');

        return $diploma->output('S');
    }
}
