<?php
declare(strict_types=1);

namespace FEC\Controllers\Api;

use Slim\Http\Request;
use Slim\Http\Response;
use FEC\Controllers\Controller;

/**
 * Class SettingsController
 * @package FEC\Controllers\Api
 */
class SettingsController extends CrudController
{
    /**
     * Update system settings.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function updateSettings(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        $settings = $this->container->get('Settings');
        $settings->updateBulk($data);

        return $resp;
    }
}
