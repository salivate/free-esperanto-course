<?php
declare(strict_types=1);

namespace FEC\Middleware;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AuthorizationMiddleware
 * @package FEC\Middleware
 */
class AuthorizationMiddleware
{
    protected $container;

    /**
     * Constructor
     *
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        $this->container = $c;
    }
    
    /**
     * Block unauthorized users.
     *
     * @param Request $req
     * @param Response $resp
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $req, Response $resp, callable $next)
    {
        $header = $req->getHeaderLine('Authorization');
        $jwt = trim((string)substr($header, strlen('Bearer ')));

        try {
            $token = $this->container->get('JWTDecoder')($jwt);

            if ($token['exp'] < time()) {
                throw new \Exception('Expired session');
            }
        }
        catch (\Exception $e) {
            return $resp->withHeader('WWW-Authenticate', 'Bearer')
                        ->withJson(['error' => 'Expired session'], 401);
        }

        $req = $req->withAttribute('token', $token)
                   ->withAttribute('account', (array)($token['account']));

        return $next($req, $resp);
    }
}
