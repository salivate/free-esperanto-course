const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = (env) => {
    return {
        entry: "./src/js/admin/main.js",
        output: {
            filename: "main.js",
            path: path.resolve(__dirname, "dist/public/js/admin"),
            sourceMapFilename: "main.js.map"
        },
        devtool: "source-map",
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    use: "vue-loader"
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: ["vue-style-loader", "css-loader", "sass-loader"]
                }
            ]
        },
        plugins: [
            new VueLoaderPlugin()
        ]
    };
};
